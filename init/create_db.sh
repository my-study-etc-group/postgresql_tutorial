#!/bin/sh

psql -U postgres -c "create database dvdrental"

# https://www.postgresqltutorial.com/postgresql-sample-database/
pg_restore -U postgres -d dvdrental /docker-entrypoint-initdb.d/dvdrental.tar
